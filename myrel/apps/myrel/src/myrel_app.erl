%%%-------------------------------------------------------------------
%% @doc myrel public API
%% @end
%%%-------------------------------------------------------------------

-module(myrel_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
    {ok, Sup} = myrel_sup:start_link(),
    {ok, MaxClients} = application:get_env(client_count),
%%     io:format("clientList = ~p ~n", [MaxClients]),
    ClientList = [list_to_atom(lists:concat([fidis_id|[I]]))||I<-lists:seq(1,MaxClients)],

%%        [get_process_info(Pid) || Pid <- processes()].
%%    io:format("clientList = ~p ~n", [ClientList]),
    lager:error("hahahhaah"),
    [start_client(ClientId, Sup) || ClientId <- ClientList],
    {ok, Sup}.


%%--------------------------------------------------------------------
stop(_State) ->
    ok.

%%====================================================================
%% Internal functions
%%====================================================================
start_client(ClientId, Sup) ->
    ChildSpec = {ClientId, {myrel_client, start_link, [ClientId]},
        temporary,
        1000,
        worker,
        [myrel_client]},
%%    io:format("ChildSpec is ~p  ~n", [ChildSpec]),
    case supervisor:start_child(Sup,ChildSpec) of
        {ok, _Child} ->  io:format("~p started ~n", [ClientId]);
        {error, Reason} ->  io:format("~p started failed , ~p  ~n", [ClientId,Reason])
    end.
%%    {ok, _Child} = supervisor:start_child(Sup,ChildSpec).
