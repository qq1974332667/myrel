%%%-------------------------------------------------------------------
%% @doc myapp top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(myrel_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

-record(state, {limit=0, refs, fidises=queue:new()}).
%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================
-define(SPEC(MFA),
    {}).

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
%%init([]) ->
%%    ClientCount = application:get_env(client_count),
%%    #state{limit = ClientCount},
%%    {ok, { {one_for_all, 5, 1000}, [{myrel_client,{myrel_client,start_link,[]},transient, 60000, worker, [myrel_client]}]} }.

init([]) ->
    {ok, {{one_for_all, 5, 1000}, []}}.



%%====================================================================
%% Internal functions
%%====================================================================
